let Stages = [
    {
        parameters: [
            {
                name: 'הגשות במרכבה',
                kpis: [
                    {
                        codeName: 'A001',
                        description: 'הצהרת מחויבות ראש עיר להובלת המיזם וקידומו',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A002',
                        description: 'הצהרה והתחייבות ראש רשות וגזבר על קיום תנאים הוראות והנחיות',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A003',
                        description: 'מילוי שאלון מדדי אפשריבריא בעיר – השלמת ההגשה המקוונת',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A004',
                        description: 'תכנית עבודה לשנת 2021 – השלמת ההגשה המקוונת',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A005',
                        description: 'תכנית עבודה שנה אחרונה 2020',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A006',
                        description: 'פרוטוקול/סיכום כל ישיבות ועדת היגוי ב-12 חודשים האחרונים כולל שמות ותפקידי משתתפים, נושאים ותוכן',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A007a',
                        description: 'תעודת השכלה של מתאם הבריאות',
                        type: 'conditional',
                        isOptional: true
                    },
                    {
                        codeName: 'A007b',
                        description: 'הוכחה רשמית / חוזה העסקה של מתאם הבריאות',
                        type: 'conditional',
                        isOptional: true
                    },
                    {
                        codeName: 'A008',
                        description: 'טפסי חובה של התכ"מ (תקנון, כספים ומשק) כפי שמפורטים בפורטל מרכב"ה',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A009a',
                        description: 'הגשת טופס 149',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A009b',
                        description: 'הגשת טופס 150',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A000',
                        description: 'התקיימות כל התנאים: k001-k006, k008-k009',
                        type: 'conclusion',
                        isOptional: false
                    }
                ]
            },
            {
                name: 'מתאם הבריאות',
                kpis: [
                    {
                        codeName: 'A011',
                        description: 'השכלה - תואר ראשון',
                        type: 'measure',
                        isOptional: false
                    },
                    {
                        codeName: 'A012',
                        description: 'השכלה - תואר שני',
                        type: 'measure',
                        isOptional: false
                    },
                    {
                        codeName: 'A013',
                        description: ' שנות וחודשי ניסיון בריכוז וניהול פרויקטים',
                        type: 'measure',
                        isOptional: false
                    },
                    {
                        codeName: 'A014',
                        description: 'קורס הכשרה למקדמי בריאות',
                        type: 'measure',
                        isOptional: false
                    },
                    {
                        codeName: 'A015',
                        description: ' וותק כמתאם בריאות או מנהל מחלקת בריאות עירונית',
                        type: 'measure',
                        isOptional: false
                    },
                    {
                        codeName: 'A016',
                        description: ' +תואר שני בקידום בריאות או בריאות הציבור שנת ניסיון בריכוז פרויקטים קהילתיים',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A017',
                        description: ' + תואר שני מהרשימה או באישור המחלקה שנתיים ניסיון בריכוז פרויקטים + קורס הכשרה',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A018',
                        description: 'תואר ראשון מהרשימה או באישור המחלקה, שנתיים ניסיון בריכוז פרויקטים וקורס הכשרה',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A019',
                        description: '6 שנים כמתאם בריאות או מנהל מחלקת בריאות עירונית וקורס הכשרה',
                        type: 'conditional',
                        isOptional: false
                    },
                    {
                        codeName: 'A010',
                        description: ':התקיימות אחד התנאים A015 - A018',
                        type: 'conclusion',
                        isOptional: false
                    },
                ]
            },
        ]
    }
]




// let Stages = {
//     stageOne: {
//         paramOne: {
//             name: 'הגשות במרכבה',
//             kpis: [
//                 {
//                     codeName: 'A001',
//                     description: 'הצהרת מחויבות ראש עיר להובלת המיזם וקידומו',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A002',
//                     description: 'הצהרה והתחייבות ראש רשות וגזבר על קיום תנאים הוראות והנחיות',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A003',
//                     description: 'מילוי שאלון מדדי אפשריבריא בעיר – השלמת ההגשה המקוונת',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A004',
//                     description: 'תכנית עבודה לשנת 2021 – השלמת ההגשה המקוונת',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A005',
//                     description: 'תכנית עבודה שנה אחרונה 2020',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A006',
//                     description: 'פרוטוקול/סיכום כל ישיבות ועדת היגוי ב-12 חודשים האחרונים כולל שמות ותפקידי משתתפים, נושאים ותוכן',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A007a',
//                     description: 'תעודת השכלה של מתאם הבריאות',
//                     type: 'conditional',
//                     isOptional: true
//                 },
//                 {
//                     codeName: 'A007b',
//                     description: 'הוכחה רשמית / חוזה העסקה של מתאם הבריאות',
//                     type: 'conditional',
//                     isOptional: true
//                 },
//                 {
//                     codeName: 'A008',
//                     description: 'טפסי חובה של התכ"מ (תקנון, כספים ומשק) כפי שמפורטים בפורטל מרכב"ה',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A009a',
//                     description: 'הגשת טופס 149',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A009b',
//                     description: 'הגשת טופס 150',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A000',
//                     description: 'התקיימות כל התנאים: k001-k006, k008-k009',
//                     type: 'conclusion',
//                     isOptional: false
//                 }
//             ]
//         },
//         paramTwo: {
//             name: 'מתאם הבריאות',
//             kpis: [
//                 {
//                     codeName: 'A011',
//                     description: 'השכלה - תואר ראשון',
//                     type: 'measure',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A012',
//                     description: 'השכלה - תואר שני',
//                     type: 'measure',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A013',
//                     description: ' שנות וחודשי ניסיון בריכוז וניהול פרויקטים',
//                     type: 'measure',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A014',
//                     description: 'קורס הכשרה למקדמי בריאות',
//                     type: 'measure',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A015',
//                     description: ' וותק כמתאם בריאות או מנהל מחלקת בריאות עירונית',
//                     type: 'measure',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A016',
//                     description: ' +תואר שני בקידום בריאות או בריאות הציבור שנת ניסיון בריכוז פרויקטים קהילתיים',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A017',
//                     description: ' + תואר שני מהרשימה או באישור המחלקה שנתיים ניסיון בריכוז פרויקטים + קורס הכשרה',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A018',
//                     description: 'תואר ראשון מהרשימה או באישור המחלקה, שנתיים ניסיון בריכוז פרויקטים וקורס הכשרה',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A019',
//                     description: '6 שנים כמתאם בריאות או מנהל מחלקת בריאות עירונית וקורס הכשרה',
//                     type: 'conditional',
//                     isOptional: false
//                 },
//                 {
//                     codeName: 'A010',
//                     description: ':התקיימות אחד התנאים A015 - A018',
//                     type: 'conclusion',
//                     isOptional: false
//                 },
//             ]
//         }
//     },
// }





// type: conditional / points / conclusion


// type: conditional / points / conclusion