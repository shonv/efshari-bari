let submissionID = "a5b67816-53ff-437d-97f6-a8e704c3389a";

// Data variables
let data = "";
let db_user = "";
let db_city = "";
let db_cityFiling = "";
let db_programs = "";

let db_ready_counter = 0;


// GUI variables
let Label_CurrentYear = "";
let Label_CurrentCity = "";
let Label_CoordinatorName = "";
let Label_CoordinatorEmail = "";
let Label_CoordinatorPhone = "";
let Label_CitizensCount = "";
let Label_SociRank = "";
let Label_PeripheralRank = "";


$(document).ready(function () {
    Label_CurrentYear = $("#CurrentYear").get(0);
    Label_CurrentCity = $("#CurrentCity").get(0);
    Label_CoordinatorName = $("#CoordinatorName").get(0);
    Label_CoordinatorEmail = $("#CoordinatorEmail").get(0);
    Label_CoordinatorPhone = $("#CoordinatorPhone").get(0);
    Label_CitizensCount = $("#CitizensCount").get(0);
    Label_SociRank = $("#SociRank").get(0);
    Label_PeripheralRank = $("#PeripheralRank").get(0);

    Initialize();
});



function Initialize() {
    // Web app's Firebase configuration
    const firebaseConfig = {
        apiKey: "AIzaSyCf9OPIAkCtDn8SMONi0-Cu5xzdTyHYYCE",
        authDomain: "chrome-oven-291602.firebaseapp.com",
        databaseURL: "https://chrome-oven-291602.firebaseio.com",
        projectId: "chrome-oven-291602",
        storageBucket: "chrome-oven-291602.appspot.com",
        messagingSenderId: "581131755858",
        appId: "1:581131755858:web:c316f9e4001ca492319a8b",
        measurementId: "G-36YK38DGCF"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    data = firebase.firestore();


    // start fetching data from each collection. 
    // When all data has been fetched, grid is drawn using CheckAllDataFetched() function.
    GetCityCoordinatorData();
    GetCityFillingData();
    GetCityData();
    GetProgramData();
}

function TestFunc() {

}


function CheckAllDataFetched() {
    // SHON- THIS FUNCTION IS OUR INITIALIZE FOR DRAWING DATA ON GRID

    // all data has been fetched
    if (db_ready_counter >= 4) {
        TestFunc();

        // CORE FUNC - Heart of this project, this func will handle all CALCULATIONS using fetched data & assign the result in my KPIs static list.
        CalculateKPIsStatus();

        console.log(Stages);

        GUI.DrawCoordinatorData(db_user);
        GUI.Stage1Parameter1DrawTable(db_cityFiling);
        GUI.Stage1Parameter2DrawTable(db_city);

        return true;
    }
}


function CalculateKPIsStatus() {
    $.each(Stages, function (i, stage) {

        $.each(stage.parameters, function (j, parameter) {

            $.each(parameter.kpis, function (k, kpi) {

                // Run on all KPIs and set its status using the data submited by the user
                switch (kpi.codeName) {
                    case "A000":
                        // shon - conclusion kpi - ignore for now
                        break;
                    case "A001":
                        kpi.status = db_cityFiling.k001MayorCommitmentFilledChecked ? true : false; break;
                    case "A002":
                        kpi.status = db_cityFiling.k002MayorDeclarationFilledChecked ? true : false; break;
                    case "A003":
                        kpi.status = db_cityFiling.k003Stage1_2_QuestionnaireFilledChecked ? true : false; break;
                    case "A004":
                        kpi.status = db_cityFiling.k004Stage3_QuestionnaireFilledChecked ? true : false; break;
                    case "A005":
                        kpi.status = db_cityFiling.k005ProgramForLastYear2020FilledChecked ? true : false; break;
                    case "A006":
                        kpi.status = db_cityFiling.k006ProtocolsForLastYearsFilledChecked ? true : false; break;
                    case "A007a":
                        kpi.status = db_cityFiling.k007A_CoordinatorEducationFilledChecked ? true : false; break;
                    case "A007b":
                        kpi.status = db_cityFiling.k007B_CoordinatorEducationFilledChecked ? true : false; break;
                    case "A008":
                        kpi.status = db_cityFiling.k008MandatoryTakamFormsFilledCheck ? true : false; break;
                    case "A009a":
                        kpi.status = db_cityFiling.k009A_Form149FilledChecked ? true : false; break;
                    case "A009b":
                        kpi.status = db_cityFiling.k009B_Form150Checked ? true : false; break;
                    case "A010":
                        // shon - conclusion kpi - ignore for now
                        break;
                    case "A011":
                        kpi.status = db_city.bachelorsDegree.answer;
                        kpi.sysReportData = db_city.bachelorsDegree.value;
                        break;
                    case "A012":
                        kpi.status = db_city.masterDegree.answer;
                        kpi.sysReportData = db_city.masterDegree.value;
                        break;
                    case "A013":
                        kpi.status = db_city.healthPromotionCourse.answer;
                        kpi.sysReportData = db_city.healthPromotionCourse.subanswer;
                        break;
                    case "A014":
                        kpi.status = db_city.coordinatorEmployedInCity ? true : false;
                        kpi.sysReportData = db_city.coordinatorEmploymentYear && db_city.coordinatorEmployedInCity ? "כן מתאריך: " + db_city.coordinatorEmploymentYear : "לא";
                        break;
                    case "A015":
                        kpi.status = db_city.managingExperience.answer ? true : false;

                        let a015SysReport = db_city.managingExperience.year ? "שנים: " + db_city.managingExperience.year : "שנים: 0";
                        a015SysReport += "<br/>";
                        a015SysReport += db_city.managingExperience.month ? "חודשים: " + db_city.managingExperience.month : "חודשים: 0";

                        kpi.sysReportData = a015SysReport;
                        break;
                    case "A016":
                        if ((db_city.masterDegree.value == "בריאות הציבור עם התמחות בקידום בריאות" || db_city.masterDegree.value == "בריאות הציבור  ") &&
                            (db_city.managingExperience.year && parseInt(db_city.managingExperience.year) >= 1) ||
                            (db_city.managingExperience.month && parseInt(db_city.managingExperience.month) >= 12)) {
                            kpi.status = true;
                        }
                        break;
                    case "A017":
                        // A: create the KPI of type input, the user (inspector) will inspec the custom value that the user entered in the form.


                        // Get value from client-side on user interaction
                        /* 
                         if masterDegree == 
                         כל מקצועות הבריאות,
                         מדעי הרפואה,
                         עו"ס קהילתית,
                         מדיניות/מנהל ציבורי,
                         איכות הסביבה,
                         פסיכולוגיה ארגונית,
                         פיזיולוגיה של הספורט,
                         וחינוך,

                        OR

                        inspector see's the KPIs custom info places (משתמש בחר "אחר" והכניס קלט משלו) and decides manually if he want to approve this KPI.
                        */
                        break;
                    case "A018":
                        // A: create the KPI of type input, the user (inspector) will inspec the custom value that the user entered in the form.


                        // Get value from client-side on user interaction
                        /* 
                         if masterDegree == 
                         כל מקצועות הבריאות,
                         מדעי הרפואה,
                         עו"ס קהילתית,
                         מדיניות/מנהל ציבורי,
                         איכות הסביבה,
                         פסיכולוגיה ארגונית,
                         פיזיולוגיה של הספורט,
                         וחינוך,

                        OR

                        inspector see's the KPIs custom info places (משתמש בחר "אחר" והכניס קלט משלו) and decides manually if he want to approve this KPI.
                        */
                        break;
                    case "A019":
                        if (db_city.coordinatorEmployedInCity && db_city.coordinatorEmploymentYear) {
                            let coordinatorEmpYear = parseInt(db_city.coordinatorEmploymentYear.split('-')[0]);

                            if (new Date().getFullYear() - 6 <= coordinatorEmpYear) {
                                kpi.status = true;
                            }
                        }
                        break;
                    case "A020":
                        // shon - conclusion kpi - ignore for now
                        break;
                    case "A021":
                        kpi.status = db_city.steeringComittee.answer ? true : false;
                        break;
                    case "A022":
                        kpi.status = parseInt(db_city.steeringComitteeConvened) >= 2 ? true : false;
                        break;
                    case "A023":
                        if (db_city.chairingPersonRole.answer && db_city.chairingPersonRole.answer != "אחר - פרט") {
                            kpi.status = true;
                        }
                        break;
                    case "A024":
                        kpi.status = db_city.steeringComitteeMembersIncluded.answers.includes("מתאם הבריאות") ? true : false;
                        break;
                    case "A025":
                        let steeringComitteeCounter = 0;
                        db_city.steeringComitteeMembersIncluded.answers.includes("נציג תושבים") ? steeringComitteeCounter++ : "";
                        db_city.steeringComitteeMembersIncluded.answers.includes("מנהל או נציג בכיר מחלקת ספורט") ? steeringComitteeCounter++ : "";
                        db_city.steeringComitteeMembersIncluded.answers.includes("מנהל או נציג בכיר מחלקת בריאות") ? steeringComitteeCounter++ : "";
                        db_city.steeringComitteeMembersIncluded.answers.includes("מנהל או נציג בכיר מחלקת חינוך") ? steeringComitteeCounter++ : "";

                        if (steeringComitteeCounter >= 3) {
                            kpi.status = true;
                        }
                        break;
                    case "A026":
                        let attendedCounter = 0;
                        db_city.steeringComitteeRepresentativeAtended.answers.includes("מקדם הבריאות מלשכת הבריאות המחוזית/ נפתית") ? attendedCounter++ : "";
                        db_city.steeringComitteeRepresentativeAtended.answers.includes("מפקח מחוזי של משרד התרבות והספורט") ? attendedCounter++ : "";
                        db_city.steeringComitteeRepresentativeAtended.answers.includes("מדריכה אזורית של משרד החינוך") ? attendedCounter++ : "";
                        db_city.steeringComitteeRepresentativeAtended.answers.includes("התזונאי המחוזי/ לשכתי של משרד הבריאות") ? attendedCounter++ : "";

                        if (attendedCounter >= 4) {
                            kpi.status = true;
                        }
                        break;
                    case "A030":
                        // shon - conclusion kpi - ignore for now    
                        break;
                    case "A031":
                        // Iterate through all plans and all activities under each plan. If found 3 plans with at least 3 activitis in each plan then true
                        let A031PlansActivitiesCOunter = 0;
                        let A031activitiesCounter = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {
                                A031activitiesCounter = 0;

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;
                                    db_programs[dynamicPlan][dynamicActivity].activityName != "" ? A031activitiesCounter++ : "";
                                }

                                if (A031activitiesCounter >= 3) {
                                    A031PlansActivitiesCOunter++;
                                }
                            }
                        }

                        if (A031PlansActivitiesCOunter >= 3) {
                            kpi.status = true;
                        }
                        break;
                    case "A032":
                        let A032activitiesCounter = 0;
                        let coronaAdaptedActivitiesCounter = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {
                                A032activitiesCounter = 0;

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    // check activity valid
                                    if (db_programs[dynamicPlan][dynamicActivity].cost.totalCost && parseInt(db_programs[dynamicPlan][dynamicActivity].cost.totalCost) > 0) {
                                        A032activitiesCounter++;

                                        // check activity is corona adapted
                                        if (db_programs[dynamicPlan][dynamicActivity].coronaAdapted != "" &&
                                            db_programs[dynamicPlan][dynamicActivity].coronaAdapted != "לא רלוונטי") {
                                            coronaAdaptedActivitiesCounter++
                                        }
                                    }
                                }

                                // at least 30% activities that are corona adapted
                                if ((A032activitiesCounter * 30 / 100) >= coronaAdaptedActivitiesCounter) {
                                    kpi.status = true;
                                }
                            }
                        }

                        break;
                    case "A033":
                        // Iterate through all plans. If found plan with "נושא עיקרי" == תזונה בריאה then תנאי סף          
                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName && db_programs[dynamicPlan].topic == "תזונה בריאה") {
                                kpi.status = true;
                                break;
                            }
                        }

                        break;
                    case "A034":
                        // Iterate through all plans. If found plan with "נושא עיקרי" == פעילות גופנית then תנאי סף
                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName && db_programs[dynamicPlan].topic == "פעילות גופנית") {
                                kpi.status = true;
                                break;
                            }
                        }
                        break;
                    case "A040":
                        // shon - conclusion kpi - ignore for now    
                        break;
                    case "A041":
                        // Get value from client-side on user interaction

                        // let merkavaSum = $("#MerkavaCostSum").val();

                        // if (merkavaSum && parseInt(merkavaSum) > 0) {
                        //     kpi.status = true;
                        // }
                        break;
                    case "A042":
                        let totalSumOfAllActivities = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    if (db_programs[dynamicPlan][dynamicActivity].cost.totalCost &&
                                        db_programs[dynamicPlan][dynamicActivity].cost.totalCost > 0) {
                                        totalSumOfAllActivities += db_programs[dynamicPlan][dynamicActivity].cost.totalCost;
                                    }
                                }

                            }
                        }

                        if (totalSumOfAllActivities != 0) {
                            kpi.status = true;
                            kpi.totalActivitiesSum = totalSumOfAllActivities;
                        }

                        break;
                    case "A043":
                        // Get value from client-side on user interaction

                        // let efshariBariSeniority = $("#EfshariBariSeniority").val();

                        // if (efshariBariSeniority && parseInt(efshariBariSeniority) > 0) {
                        //     kpi.status = true;
                        // }
                        break;
                    case "A044":
                        // shon - will be discussed later how to handle
                        break;
                    case "A045":
                        // shon - will be discussed later how to handle
                        break;
                    case "B000":
                        // shon - conclusion KPI - ignore for now        
                        break;
                    case "B001":
                        // shon - will be discussed later how to handle
                        break;
                    case "B002":
                        // shon - will be discussed later how to handle
                        break;
                    case "C000":
                        // shon - conclusion KPI - ignore for now    
                        break;
                    case "C001":
                        // Iterate through all plans and all activities under each plan. If found 3 plans with at least 3 activitis in each plan then true
                        let C001PlansActivitiesCOunter = 0;
                        let activitiesCounter = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            activitiesCounter = 0;

                            for (let j = 1; j < 9; j++) {
                                let dynamicActivity = "activity" + j;

                                if (db_programs[dynamicPlan][dynamicActivity].cost.totalCost &&
                                    db_programs[dynamicPlan][dynamicActivity].cost.totalCost > 0) {
                                    activitiesCounter++;
                                }
                            }

                            if (activitiesCounter >= 3) {
                                C001PlansActivitiesCOunter++;
                            }
                        }

                        if (C001PlansActivitiesCOunter >= 3) {
                            kpi.status = true;
                        }
                        break;
                    case "C002":
                        let C002activitiesCounter = 0;
                        let coronaAdaptedActivitiesCounter = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            activitiesCounter = 0;

                            for (let j = 1; j < 9; j++) {
                                let dynamicActivity = "activity" + j;

                                // check activity valid
                                if (db_programs[dynamicPlan][dynamicActivity].cost.totalCost &&
                                    db_programs[dynamicPlan][dynamicActivity].cost.totalCost > 0) {
                                    C002activitiesCounter++;

                                    // check activity is corona adapted
                                    if (db_programs[dynamicPlan][dynamicActivity].coronaAdapted != "" &&
                                        db_programs[dynamicPlan][dynamicActivity].coronaAdapted != "לא רלוונטי") {
                                        coronaAdaptedActivitiesCounter++
                                    }
                                }
                            }

                            // at least 30% activities that are corona adapted
                            if ((C002activitiesCounter * 30 / 100) >= coronaAdaptedActivitiesCounter) {
                                kpi.status = true;
                            }
                        }

                        break;
                    case "C003":
                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName && db_programs[dynamicPlan].topic == "תזונה בריאה") {
                                kpi.status = true;
                                break;
                            }
                        }

                        break;
                    case "C004":
                        // Iterate through all plans. If found plan with "נושא עיקרי" == פעילות גופנית then תנאי סף
                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName && db_programs[dynamicPlan].topic == "פעילות גופנית") {
                                kpi.status = true;
                                break;
                            }
                        }
                        break;
                    case "C005":
                        kpi.status = db_cityFiling.k005ProgramForLastYear2020FilledChecked ? true : false; break;
                    case "C006a":
                        let tzunaProgramsWithoutNutritionistCount = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].topic && db_programs[dynamicPlan].topic == "תזונה בריאה") {

                                let flag = false;

                                for (let j = 0; j < db_programs[dynamicPlan].professionalFactors.length; j++) {

                                    if (db_programs[dynamicPlan].professionalFactors[j] == "תזונאית בלשכת הבריאות הנפתית \ מחוזית" ||
                                        db_programs[dynamicPlan].professionalFactors[j] == "תזונאי המועסק בקביעות על ידי הרשות (תזונאי עירוני)" ||
                                        db_programs[dynamicPlan].professionalFactors[j] == "תזונאי\ת בעל תעודת רישוי (לא מועסק בקביעות על ידי הרשות)") {
                                        flag = true;
                                        break;
                                    }
                                }

                                if (flag) {
                                    tzunaProgramsWithoutNutritionistCount++;
                                }
                            }
                        }

                        // If i found at least 1 program of type "תזונה בריאה" that has no nutritionist, set status == false
                        if (tzunaProgramsWithoutNutritionistCount == 0) {
                            kpi.status = true;
                        }


                        break;
                    case "C006b":
                        let sportProgramsWithoutNutritionistCount = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].topic && db_programs[dynamicPlan].topic == "פעילות גופנית") {

                                let flag = false;

                                for (let j = 0; j < db_programs[dynamicPlan].professionalFactors.length; j++) {

                                    if (db_programs[dynamicPlan].professionalFactors[j] == "תזונאית בלשכת הבריאות הנפתית \ מחוזית" ||
                                        db_programs[dynamicPlan].professionalFactors[j] == "תזונאי המועסק בקביעות על ידי הרשות (תזונאי עירוני)" ||
                                        db_programs[dynamicPlan].professionalFactors[j] == "תזונאי\ת בעל תעודת רישוי (לא מועסק בקביעות על ידי הרשות)") {
                                        flag = true;
                                        break;
                                    }
                                }

                                if (flag) {
                                    sportProgramsWithoutNutritionistCount++;
                                }
                            }
                        }

                        // If all programs of type "פעילות גופנית" had nutritionist - set kpi to true
                        if (sportProgramsWithoutNutritionistCount == 0) {
                            kpi.status = true;
                        }
                        break;
                    case "C007a":
                        if (db_city.steeringComitteeConvened && parseInt(db_city.steeringComitteeConvened) >= 2) {
                            kpi.status = true;
                        }
                        break;
                    case "C007b":
                        // Get value from client-side on user interaction
                        break;
                    case "c008":
                        // Get value from client-side on user interaction

                        // let merkavaSum = $("#MerkavaCostSum").val();

                        // if (merkavaSum && parseInt(merkavaSum) > 0) {
                        //     kpi.status = true;
                        // }
                        break;
                    case "c009":
                        let activitiesC009Count = 0;

                        let envirChangeStrats = [
                            "הקמת שביל או מסלול הליכה",
                            "הקמת שביל או מסלול אופניים",
                            "הקמת מתקן",
                            "הקמת גינה קהילתית",
                            "הוספת תאורה",
                            "הוספת הצללה",
                            "הוספת הושבה",
                            "הוספת ברזיות",
                            "הוספת שילוט",
                            "רכישת ציוד קבוע למסגרת",
                        ];

                        let policyStrats = [
                            "כתיבת נהלים",
                            "מסמך מדיניות",
                            "מכרז",
                            "אכיפה",
                            "מסמכי תכנון אסטרטגי",
                            'החלטה תקציבית (תוספת או תמרוץ תקציבי, הוספת שעות פ"ג בביס)',
                            'הגדלת כ"א לתחום',
                        ];

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            for (let j = 1; j < 9; j++) {
                                let dynamicActivity = "activity" + j;

                                // check activity valid
                                if (db_programs[dynamicPlan][dynamicActivity].cost.totalCost &&
                                    db_programs[dynamicPlan][dynamicActivity].cost.totalCost > 0) {

                                    // check activity strategy of specific types, if found, add to count.
                                    if (envirChangeStrats.indexOf(db_programs[dynamicPlan][dynamicActivity].activityStrategy) > -1 ||
                                        policyStrats.indexOf(db_programs[dynamicPlan][dynamicActivity].activityStrategy) > -1
                                    ) {
                                        activitiesC009Count++;
                                    }
                                }
                            }

                            // more than 4 activities that are of specified types.
                            if (activitiesC009Count > 4) {
                                kpi.status = true;
                            }
                        }




                        break;
                    case "C010":
                        // shon - conclusion kpi - ignore for now        
                        break;
                    case "C011":
                        // Get value from client-side on user interaction
                        break;
                    case "C012":
                        // Get value from client-side on user interaction
                        break;
                    case "C020":
                        // shon - conclusion kpi - ignore for now        
                        break;
                    case "C021":
                        // Get value from client-side on user interaction    
                        break;
                    case "C022":
                        // Get value from client-side on user interaction    
                        break;
                    case "C030":
                        // shon - conclusion kpi - ignore for now        
                        break;
                    case "C031":
                        if (db_programs.healthBureausRepresentatives) {
                            kpi.status = true;
                        }

                        break;
                    case "C032":
                        let programFlag = false;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            for (let j = 1; j < 9; j++) {
                                let dynamicActivity = "activity" + j;

                                if (db_programs[dynamicPlan][dynamicActivity].cost.totalCost &&
                                    db_programs[dynamicPlan][dynamicActivity].cost.totalCost > 0) {

                                    $.each(db_programs[dynamicPlan][dynamicActivity].professionalFactors, function (indx, professional) {
                                        if (professional == "נציג מחוזי \ ארצי של משרד התרבות והספורט" || professional == "מנהל מחלקת ספורט ברשות") {
                                            programFlag = true;
                                            return false;
                                        }
                                    })
                                }

                                if (programFlag) {
                                    break;
                                }
                            }

                            if (programFlag) {
                                break;
                            }
                        }

                        if (programFlag) {
                            kpi.status = true;
                        }
                        break;
                    case "C040":
                        // shon - conclusion kpi - ignore for now        
                        break;
                    case "C041":
                        let C041PopulationCounter = 0;
                        let popToCheck = [
                            "חרדים",
                            "ערבים",
                            "אוכלוסיות בסיכון",
                            "בני נוער",
                            " מבוגרים מעל גיל 65"
                        ];

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].targetPopulationSelect) > -1) {
                                        C041PopulationCounter++;
                                        continue;
                                    }

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].age) > -1) {
                                        C041PopulationCounter++;

                                        // handle specific case for old people abobe 65 age.
                                        if (db_programs[dynamicPlan].topic == "פעילות גופנית" && db_programs[dynamicPlan][dynamicActivity].age == " מבוגרים מעל גיל 65") {
                                            C041PopulationCounter--;
                                        }

                                        continue;
                                    }
                                }
                            }
                        }

                        if (C041PopulationCounter >= 6) {
                            kpi.status = true;
                            kpi.points = 12;
                        }


                        break;
                    case "C042":
                        let C042PopulationCounter = 0;
                        let popToCheck = [
                            "חרדים",
                            "ערבים",
                            "אוכלוסיות בסיכון",
                            "בני נוער",
                            " מבוגרים מעל גיל 65"
                        ];

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].targetPopulationSelect) > -1) {
                                        C042PopulationCounter++;
                                        continue;
                                    }

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].age) > -1) {
                                        C042PopulationCounter++;

                                        // handle specific case for old people abobe 65 age.
                                        if (db_programs[dynamicPlan].topic == "פעילות גופנית" && db_programs[dynamicPlan][dynamicActivity].age == " מבוגרים מעל גיל 65") {
                                            C042PopulationCounter--;
                                        }

                                        continue;
                                    }
                                }
                            }
                        }

                        if (C042PopulationCounter >= 4 && C042PopulationCounter <= 6) {
                            kpi.status = true;
                            kpi.points = 8;
                        }
                        break;
                    case "C043":
                        let C043PopulationCounter = 0;
                        let popToCheck = [
                            "חרדים",
                            "ערבים",
                            "אוכלוסיות בסיכון",
                            "בני נוער",
                            " מבוגרים מעל גיל 65"
                        ];

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].targetPopulationSelect) > -1) {
                                        C043PopulationCounter++;
                                        continue;
                                    }

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].age) > -1) {
                                        C043PopulationCounter++;

                                        // handle specific case for old people abobe 65 age.
                                        if (db_programs[dynamicPlan].topic == "פעילות גופנית" && db_programs[dynamicPlan][dynamicActivity].age == " מבוגרים מעל גיל 65") {
                                            C043PopulationCounter--;
                                        }

                                        continue;
                                    }
                                }
                            }
                        }

                        if (C043PopulationCounter < 4) {
                            kpi.status = true;
                            kpi.points = 0;
                        }
                        break;
                    case "C044":
                        let C044PopulationCounter = 0;
                        let popToCheck = [
                            "חרדים",
                            "ערבים",
                            "אוכלוסיות בסיכון",
                            "בני נוער",
                            " מבוגרים מעל גיל 65"
                        ];

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].targetPopulationSelect) > -1) {
                                        C044PopulationCounter++;
                                        continue;
                                    }

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        popToCheck.indexOf(db_programs[dynamicPlan][dynamicActivity].age) > -1) {
                                        C044PopulationCounter++;

                                        // handle specific case for old people abobe 65 age.
                                        if (db_programs[dynamicPlan].topic == "פעילות גופנית" && db_programs[dynamicPlan][dynamicActivity].age == " מבוגרים מעל גיל 65") {
                                            C044PopulationCounter--;
                                        }

                                        continue;
                                    }
                                }
                            }
                        }

                        if (C044PopulationCounter == 0) {
                            kpi.status = true;
                            kpi.points = 0;
                        }

                        break;
                    case "C050":
                        // shon - conclusion kpi - ignore for now        
                        break;
                    case "C051":
                        let flag65Plus = flase;
                        let activity65PlusCounter = 0;

                        for (let i = 1; i < 9; i++) {
                            let dynamicPlan = "plan" + i;

                            if (db_programs[dynamicPlan].planName) {

                                $.each(db_programs[dynamicPlan].organizationsInCity.answers, function (indx, answer) {
                                    if (answer == "מועדונים לגיל + 65") {
                                        flag65Plus = true;
                                        return false;
                                    }
                                })

                                if (flag65Plus) {
                                    break;
                                }

                                for (let j = 1; j < 9; j++) {
                                    let dynamicActivity = "activity" + j;

                                    if (db_programs[dynamicPlan][dynamicActivity].activityName &&
                                        db_programs[dynamicPlan][dynamicActivity].age == " מבוגרים מעל גיל 65") {
                                        activity65PlusCounter++;
                                    }
                                }

                                if (activity65PlusCounter >= 3) {
                                    break;
                                }
                            }
                        }

                        if (flag65Plus || activity65PlusCounter >= 3) {
                            kpi.status = true;
                            kpi.points = 5;
                        }

                        break;
                    case "C052":
                        // Get value from client-side on user interaction 

                        // if (digitalAssetsCounter >= 3) {
                        //     kpi.status = true;
                        //     kpi.points = 1;
                        // }
                        break;
                    case "C053":
                        // more than 3 digital platforms.
                        let digitalAssetsCounter = 0;
                        db_city.digitalAssets.application.answer ? digitalAssetsCounter++ : "";
                        db_city.digitalAssets.facebookPage.answer ? digitalAssetsCounter++ : "";
                        db_city.digitalAssets.others.answer ? digitalAssetsCounter++ : "";
                        db_city.digitalAssets.socialNetworks.answer ? digitalAssetsCounter++ : "";
                        db_city.digitalAssets.website.answer ? digitalAssetsCounter++ : "";
                        db_city.digitalAssets.newsletter ? digitalAssetsCounter++ : "";
                        db_city.digitalAssets.newspaper ? digitalAssetsCounter++ : "";

                        if (digitalAssetsCounter >= 3) {
                            kpi.status = true;
                            kpi.points = 4;
                        }

                        break;
                    case "C060":
                        // shon - conclusion kpi - ignore for now      
                        break;
                    case "C061":
                        kpi.sysReportData = db_city.masterDegree.value;
                        break;
                    case "C062":
                        kpi.sysReportData = db_user.localAuthorityName.cityPop2020;
                        break;
                    case "C063":
                        kpi.sysReportData = db_city.employmentRate;
                        break;
                    case "C064":
                        if (db_city.masterDegree.value == "בריאות הציבור עם התמחות בקידום בריאות" || db_city.masterDegree.value == "בריאות הציבור  ") {
                            kpi.status = true;
                            kpi.points = 4;
                        }
                        break;
                    case "C065":
                        if (db_city.employmentRate && parseInt(db_city.employmentRate) >= 50 && parseInt(db_city.employmentRate) <= 75) {
                            kpi.status = true;
                            kpi.points = 2;
                        }
                        break;
                    case "C066":
                        if (db_user.localAuthorityName.cityPop2020 && parseFloat(db_user.localAuthorityName.cityPop2020) >= 100) {
                            // big city

                            if (db_city.employmentRate && parseInt(db_city.employmentRate) >= 100) {
                                kpi.status = true;
                                kpi.points = 2;
                            }
                        }
                        else {
                            // small city

                            if (db_city.employmentRate && parseInt(db_city.employmentRate) > 75) {
                                kpi.status = true;
                                kpi.points = 2;
                            }
                        }
                        break;
                    case "C067":
                        if (db_city.cordinatorRoleInEmergency.answer &&
                            db_city.cordinatorRoleInEmergency.answer == "שותף לשלחן מל``ח/ חלק מהצוות שאחראי על בריאות בחירום" ||
                            db_city.cordinatorRoleInEmergency.answer == "ממונה/ אחראי על בריאות בחירום ") {
                            kpi.status = true;
                            kpi.points = 4;
                        }
                        break;
                    case "C070":
                        // shon - conclusion kpi - ignore for now      
                        break;
                    case "C071":
                        if (db_city.healthPolicies.disabledPopulationArea && db_city.healthPolicies.disabledPopulationArea == "יש מדיניות המיושמת במלואה") {
                            kpi.status = true;
                            kpi.points = 1;
                        }
                        break;
                    case "C072":
                        if (db_city.healthPolicies.schoolActivityHours && db_city.healthPolicies.schoolActivityHours == "יש מדיניות המיושמת במלואה") {
                            kpi.status = true;
                            kpi.points = 1;
                        }
                        break;
                    case "C073":
                        if (db_city.healthPolicies.healthyRefreshments && db_city.healthPolicies.healthyRefreshments == "יש מדיניות המיושמת במלואה") {
                            kpi.status = true;
                            kpi.points = 1;
                        }
                        break;
                    case "C074":
                        if (db_city.healthPolicies.municipalSafeFood && db_city.healthPolicies.municipalSafeFood == "יש מדיניות המיושמת במלואה") {
                            kpi.status = true;
                            kpi.points = 1;
                        }
                        break;
                    case "C075":
                        if (db_city.healthPolicies.controlingFoodSupply && db_city.healthPolicies.controlingFoodSupply == "יש מדיניות המיושמת במלואה") {
                            kpi.status = true;
                            kpi.points = 1;
                        }
                        break;
                    case "C076":
                        if (db_city.healthPolicies.smokingBanLaw && db_city.healthPolicies.smokingBanLaw == "יש מדיניות המיושמת במלואה") {
                            kpi.status = true;
                            kpi.points = 2;
                        }
                        break;
                    case "C080":
                        // shon - conclusion kpi - ignore for now      
                        break;
                    case "C081": break;
                    case "C082": break;

                    default:
                        break;
                }




            });

        });

    })
}



function GetCityCoordinatorData() {
    data.collection("users").where("submissionId", "==", submissionID).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db_user = doc.data();
                db_ready_counter++;
                CheckAllDataFetched();
            });
        })
        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
}



function GetCityFillingData() {
    data.collection("cityFilings").where("submissionId", "==", submissionID).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db_cityFiling = doc.data();
                db_ready_counter++;
                CheckAllDataFetched();
            });
        })
        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
}



function GetCityData() {
    data.collection("cities").where("submissionId", "==", submissionID).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db_city = doc.data();
                db_ready_counter++;
                CheckAllDataFetched();
            });
        })
        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
}



function GetProgramData() {
    data.collection("programs").where("submissionId", "==", submissionID).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db_programs = doc.data();
                db_ready_counter++;
                CheckAllDataFetched();
            });
        })
        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
}



