const StaticHtmlTemplates = {
    stageOne_kpi: `<div class="row">
                       <div class="col">
                           <div class="kpi-container">
                               <div class="kpi-symbol {kpi-color}">{kpi-codename}</div>
                               <div class="kpi-description">
                                   {kpi-description}
                               </div>
                           </div>
                           <div class="kpi-not-obligatory">
                                {kpi-not-obligatory}
                           </div>
                       </div>
                       <div class="col data-reported">
                       </div>
                       <div class="col is-suffecient">
                           <img src="images/{kpi-icon-filename}" />
                       </div>
                       <div class="col inspector-approval">
                           <input type="checkbox" />
                       </div>
                       <div class="col inspector-notes">
                           <input type="text" placeholder="הערה" />
                       </div>
                       <div class="col not-approved-for-budget">
                           לא אושר
                       </div>
                       <div class="col kpi-rating">
                           ללא
                       </div>
                   </div>`,

    stageTwo_kpi: `<div class="row">
                   <div class="col">
                       <div class="kpi-container">
                           <div class="kpi-symbol kpi-light-grey">{kpi-codename}</div>
                           <div class="kpi-description">
                               {kpi-description}
                           </div>
                       </div>
               
                       <div class="kpi-not-obligatory"></div>
                   </div>
               
                   <div class="col data-reported">
                       {kpi-data-reported}
                   </div>
               
                   <div class="col is-suffecient">
                   </div>
               
                   <div class="col inspector-approval">
                   </div>
               
                   <div class="col inspector-notes">
                   </div>
               
                   <div class="col not-approved-for-budget">
                   </div>
               
                   <div class="col kpi-rating">
                   </div>
               </div>`

};


class GUI {

    static DrawCoordinatorData(_db_user) {
        // Draw first section - coordinator raw data

        $(Label_CurrentYear).text(new Date().getFullYear());
        $(Label_CurrentCity).text(_db_user.localAuthorityName.cityFullName);
        $(Label_CoordinatorName).text(_db_user.firstName + " " + _db_user.lastName);
        $(Label_CoordinatorEmail).text(_db_user.email);
        $(Label_CoordinatorPhone).text(_db_user.phone);

        $(Label_CitizensCount).text(_db_user.localAuthorityName.cityPop2020);
        $(Label_SociRank).text(_db_user.localAuthorityName.citySociRank2019);
        $(Label_PeripheralRank).text(_db_user.localAuthorityName.cityPeripheralRank2019);
    }



    static Stage1Parameter1DrawTable(_db_cityFiling) {
        $.each(Stages[0].parameters[0].kpis, function (i, kpi) {
            let kpiHtml = StaticHtmlTemplates.stageOne_kpi;
            let flag = false;

            switch (kpi.codeName) {
                case "A001":
                    flag = _db_cityFiling.k001MayorCommitmentFilledChecked ? true : false;
                    break;
                case "A002":
                    flag = _db_cityFiling.k002MayorDeclarationFilledChecked ? true : false;
                    break;
                case "A003":
                    flag = _db_cityFiling.k003Stage1_2_QuestionnaireFilledChecked ? true : false;
                    break;
                case "A004":
                    flag = _db_cityFiling.k004Stage3_QuestionnaireFilledChecked ? true : false;
                    break;
                case "A005":
                    flag = _db_cityFiling.k005ProgramForLastYear2020FilledChecked ? true : false;
                    break;
                case "A006":
                    flag = _db_cityFiling.k006ProtocolsForLastYearsFilledChecked ? true : false;
                    break;
                case "A007a":
                    flag = _db_cityFiling.k007A_CoordinatorEducationFilledChecked ? true : false;
                    break;
                case "A007b":
                    flag = _db_cityFiling.k007B_CoordinatorEducationFilledChecked ? true : false;
                    break;
                case "A008":
                    flag = _db_cityFiling.k008MandatoryTakamFormsFilledCheck ? true : false;
                    break;
                case "A009a":
                    flag = _db_cityFiling.k009A_Form149FilledChecked ? true : false;
                    break;
                case "A009b":
                    flag = _db_cityFiling.k009B_Form150Checked ? true : false;
                    break;
                case "A000":
                    // console.log('found A000');
                    return;
            }

            if (flag) {
                kpiHtml = kpiHtml.replace("{kpi-color}", "kpi-missing-data");
                kpiHtml = kpiHtml.replace("{kpi-icon-filename}", "missing-information.png");
            }
            else {
                kpiHtml = kpiHtml.replace("{kpi-color}", "kpi-not-approved");
                kpiHtml = kpiHtml.replace("{kpi-icon-filename}", "unchecked-red.png");
            }

            if (kpi.isOptional) {
                kpiHtml = kpiHtml.replace("{kpi-not-obligatory}", "לא חובה");
            }
            else {
                kpiHtml = kpiHtml.replace("{kpi-not-obligatory}", ""); // remove "לא חובה" text if {kpi-not-obligatory} was found.
            }

            kpiHtml = kpiHtml.replace("{kpi-codename}", kpi.codeName);
            kpiHtml = kpiHtml.replace("{kpi-description}", kpi.description);

            $("#AgashotMerkava .table-body").append(kpiHtml);
        });


    }



    static Stage1Parameter2DrawTable(_db_cities) {
        $.each(Stages[0].parameters[1].kpis, function (i, kpi) {
            let kpiHtml = ""
            let kpiSysDataReport

            if (kpi.type == "measure") {
                kpiHtml = StaticHtmlTemplates.stageTwo_kpi;
            }
            else {
                kpiHtml = StaticHtmlTemplates.stageOne_kpi;
            }

            switch (kpi.codeName) {
                case "A011":
                    let firstDegree = ""; // get degree name from [_db_cities]
                    kpiHtml = kpiHtml.replace("{kpi-data-reported}", firstDegree);
                    break;
                case "A012":
                    let secondDegree = ""; // get degree name from [_db_cities]
                    kpiHtml = kpiHtml.replace("{kpi-data-reported}", secondDegree);
                    break;
                case "A013":
                    let experienceYears = 0; // get from [_db_cities]
                    let experienceMonths = 0; // get from [_db_cities]
                    kpiSysDataReport = "שנים: " + experienceYears + " <br>" + "חודשים: " + experienceMonths;

                    kpiHtml = kpiHtml.replace("{kpi-data-reported}", kpiSysDataReport);
                    break;
                case "A014":
                    kpiSysDataReport = "לא";
                    let doesHaveHealthCourse = false; // get degree name from [_db_cities]

                    if (doesHaveHealthCourse) {
                        kpiSysDataReport = "כן";
                    }

                    kpiHtml = kpiHtml.replace("{kpi-data-reported}", kpiSysDataReport);
                    break;
                case "A015":
                    kpiSysDataReport = "לא";
                    let seniorityAsHealthCoordinator = false; // get degree name from [_db_cities]

                    if (seniorityAsHealthCoordinator) {
                        kpiSysDataReport = "כן";
                    }

                    kpiHtml = kpiHtml.replace("{kpi-data-reported}", kpiSysDataReport);
                    break;
                case "A016":

                    // pesudo
                    if (false) {
                        kpiHtml = kpiHtml.replace("{kpi-color}", "kpi-missing-data");
                        kpiHtml = kpiHtml.replace("{kpi-icon-filename}", "missing-information.png");
                    }

                    break;

            }

            kpiHtml = kpiHtml.replace("{kpi-codename}", kpi.codeName);
            kpiHtml = kpiHtml.replace("{kpi-description}", kpi.description);

            if (kpi.isOptional) {
                kpiHtml = kpiHtml.replace("{kpi-not-obligatory}", "לא חובה");
            }
            else {
                kpiHtml = kpiHtml.replace("{kpi-not-obligatory}", ""); // remove "לא חובה" text if {kpi-not-obligatory} was found.
            }

            $("#MetaemBriut .table-body").append(kpiHtml);
        });
    }

}